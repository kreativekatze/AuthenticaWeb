﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using authenticawebapp.Models;
using authenticawebapp.Handlers;

namespace authenticawebapp.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            // simple login: check for logged in:
            if (Session["authenticated"]==null)
                return RedirectToAction("Login", "Account");


            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(RegexViewModel rvm)
        {
            try
            {
                // Result is expecting type RegexResultModel
                ViewBag.Result = RegexTester.Execute(rvm);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("regex", $"There is a problem with the regex expression: {ex.Message}");
                return View(rvm);
            }

            return View(rvm);
        }



    }
}