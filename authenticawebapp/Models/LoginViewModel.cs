﻿using System.ComponentModel.DataAnnotations;

namespace authenticawebapp.Models
{
    public class LoginViewModel
    {
        [Display(Name="Email Address", Prompt = "Email Address")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Display(Name="Password", Prompt = "Password")]
        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }
    }
}