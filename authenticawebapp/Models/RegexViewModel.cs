﻿using System.ComponentModel.DataAnnotations;

namespace authenticawebapp.Models
{
    public class RegexViewModel
    {
        [Display(Name="Regex Expression", Prompt="Regex Expression")]
        [Required]
        public string regex { get; set; }

        [Display(Name="Test String", Prompt = "Test String")]
        [Required]
        public string test { get; set; }

    }
}