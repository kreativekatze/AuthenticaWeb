﻿using authenticawebapp.Models;
using System.Text.RegularExpressions;

namespace authenticawebapp.Handlers
{
    public static class RegexTester
    {
        public static RegexResultModel Execute(RegexViewModel rvm)
        {
            // no try-catch here, exception will be passed up to Controller so it can be added to ModelState
            Match test = Regex.Match(rvm.test, @rvm.regex);

            // set the message and cssclass of the resulting expression match:
            if (test.Success)
            {
               return new RegexResultModel()
                {
                    message = $"The string <b>{test.Value}</b> matches the expression.",
                    cssclass = "success" // bootstrap alert classes
                };
            }
            else
            {
                // result does not match
                return new RegexResultModel()
                {
                    message = "The string failed the test",
                    cssclass = "danger" // bootstrap alert classes
                };
            }
        }

    }
}